#include "incremental_registration.h"
#include <vector>

int main(int argc, char** argv) {
  auto leaf_size = 0.1;
  auto downsample = true;
  auto incrReg = IncrementalRegistration{100};

  auto data = incrReg.loadData(argc, argv);

  // Check user input
  if (data.empty()) {
    PCL_ERROR("Syntax is: %s <source.pcd> <target.pcd> [*]\n", argv[0]);
    PCL_ERROR("[*] - multiple files can be added. ");
    return (-1);
  }
  PCL_INFO("Loaded %d datasets. \n", data.size());

  auto cloud_WRT_firstCloud = boost::make_shared<PointCloud>();

  for (size_t i = 1; i < data.size(); ++i) {
    PCD& source = data[i - 1];
    PCD& target = data[i];

    PCL_INFO("*********************************************** \n");
    PCL_INFO("Type 'j' or 't' to select joystick or trackball. \n");
    PCL_INFO("Type 'c' or 'a' to select camera or actor. \n");
    PCL_INFO("Move the green cloud! \n");
    PCL_INFO("*********************************************** \n");

    // Add visualization data
    incrReg.showCloudsLeft(target, source);

    // Get Cloud Actor Map (boost::unordered_map) that allow us to manipulate
    // visualizer's actors
    auto p = incrReg.getVisualizer();
    auto cloud_actor_map = p->getCloudActorMap();
    // Get the transformation from viewer and apply to my Cloud
    auto T_target_WRT_mouse_VTK =
        cloud_actor_map->at(target.f_name).viewpoint_transformation_;

    auto T_target_WTR_mouse = Eigen::Matrix4f{};
    PCLVisualizer::convertToEigenMatrix(T_target_WRT_mouse_VTK,
                                        T_target_WTR_mouse);
    pcl::transformPointCloud(*target.cloud, *target.cloud, T_target_WTR_mouse);

    PCL_INFO("Aligning %s (%d) with %s (%d).\n", source.f_name.c_str(),
             source.cloud->points.size(), target.f_name.c_str(),
             target.cloud->points.size());
    auto alignedPCD =
        incrReg.pairAlign(source.cloud, target.cloud, leaf_size, downsample);

    // transform current pair into the global transform
    pcl::copyPointCloud(*(alignedPCD.cloud), *cloud_WRT_firstCloud);

    target.Ti = alignedPCD.Ti * T_target_WTR_mouse;
    target.cloud = cloud_WRT_firstCloud;

    // write on file obtained transformation
    ofstream myfile;
    myfile.open("transformations.txt", std::ios_base::app);
    myfile << "\n";
    myfile << "from " << pcl::getFilenameWithoutPath(target.f_name)
           << " with respect to " << pcl::getFilenameWithoutPath(argv[1])
           << "\n";
    myfile << target.Ti;
    // myfile << "\n Transformation by hand: \n" << T_target_WTR_mouse;
    myfile << "\n";
    myfile.close();

    // save aligned pair, transformed into the first cloud's frame
    std::string fileName{"Output_" + std::to_string(i) + ".pcd"};

    PCL_INFO("Saving pcd file to: %s \n", fileName.c_str());
    pcl::io::savePCDFile(fileName, *cloud_WRT_firstCloud, true);
  }
}
