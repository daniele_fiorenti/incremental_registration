cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

IF(NOT CMAKE_BUILD_TYPE)
  SET(CMAKE_BUILD_TYPE RelWithDebInfo CACHE STRING
      "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel.")
ENDIF(NOT CMAKE_BUILD_TYPE)
message("CMAKE_BUILD_TYPE = ${CMAKE_BUILD_TYPE}")

SET(CMAKE_CXX_STANDARD 14)

SET(WARNING_FLAGS "-fprofile-arcs -ftest-coverage")
# SET(SANITIZER_FLAGS "-fsanitize=address -fno-omit-frame-pointer")

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${WARNING_FLAGS} ${SANITIZER_FLAGS}")

project(Incremental_Pairwise_Registration_with_Hand_Initialization)

find_package(PCL 1.7 REQUIRED)

include_directories(${PCL_INCLUDE_DIRS})

message(STATUS ${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})
add_definitions(-Wno-deprecated -DEIGEN_DISABLE_UNALIGNED_ARRAY_ASSERT)

include_directories(${PROJECT_SOURCE_DIR})

add_executable (incremental_registration main.cpp incremental_registration.cpp)
target_link_libraries (incremental_registration ${PCL_LIBRARIES})
