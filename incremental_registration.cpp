#include "incremental_registration.h"
#include <boost/filesystem.hpp>
#include <boost/algorithm/string/predicate.hpp>

using pcl::visualization::PointCloudColorHandlerCustom;
using pcl::visualization::PointCloudColorHandlerGenericField;

namespace fs = boost::filesystem;

const auto MAX_DIST = 0.5; // orig 0.1

// Define a new point representation for < x, y, z, curvature >
class MyPointRepresentation : public pcl::PointRepresentation<PointNormalT> {
  using pcl::PointRepresentation<PointNormalT>::nr_dimensions_;

 public:
  MyPointRepresentation() {
    // Define the number of dimensions
    nr_dimensions_ = 4;
  }

  // Override the copyToFloatArray method to define our feature vector
  virtual void copyToFloatArray(const PointNormalT& p, float* out) const {
    // < x, y, z, curvature >
    out[0] = p.x;
    out[1] = p.y;
    out[2] = p.z;
    out[3] = p.curvature;
  }
};

IncrementalRegistration::IncrementalRegistration(std::size_t maxIterations)
    : view_{std::make_unique<PCLVisualizer>(
          "Pairwise Incremental Registration")}, 
          maxIterations_{maxIterations} {
  view_->createViewPort(0.0, 0, 0.5, 1.0, vp1_);
  view_->createViewPort(0.5, 0, 1.0, 1.0, vp2_);

  // Add coordinate system for viewport 1
  view_->addCoordinateSystem(0.4, "rightViewport", vp1_);
  // Set view from above
  view_->setCameraPosition(0, 0, 15, // pos x, y, z
                           0, 0, 0,  // view x, y, z
                           0, 0, 0); // up x, y, z

  // Initialize new interactor style and set it to TrackballCamera
  auto interactor_style = vtkSmartPointer<vtkInteractorStyleSwitch>::New();
  interactor_style->SetCurrentStyleToTrackballCamera();
  // Change InteractorStyle to vtkInteractorStyleSwitch

  view_->setupInteractor(view_->getRenderWindow()->GetInteractor(),
                         view_->getRenderWindow(), interactor_style);
}

////////////////////////////////////////////////////////////////////////////////
/** \brief Display source and target on the first viewport of the visualizer
 *
 */
void IncrementalRegistration::showCloudsLeft(const PCD& cloud_target,
                                             const PCD& cloud_source) {
  view_->removeAllPointClouds();

  auto tgt_h =
      PointCloudColorHandlerCustom<PointT>{cloud_target.cloud, 0, 255, 0};
  auto src_h =
      PointCloudColorHandlerCustom<PointT>{cloud_source.cloud, 255, 0, 0};
  view_->addPointCloud(cloud_target.cloud, tgt_h, cloud_target.f_name, vp1_);
  view_->addPointCloud(cloud_source.cloud, src_h, cloud_source.f_name, vp1_);

  // The Green cloud can be moved:
  view_->getCloudActorMap()->at(cloud_target.f_name).actor->PickableOn();
  // The Red cloud can't be moved:
  view_->getCloudActorMap()->at(cloud_source.f_name).actor->PickableOff();

  PCL_INFO("Press q to begin the registration. \n");
  view_->spin();
}

////////////////////////////////////////////////////////////////////////////////
/** \brief Display source and target on the second viewport of the visualizer
 *
 */
void IncrementalRegistration::showCloudsRight(
    const PointCloudWithNormals::Ptr& cloud_target,
    const PointCloudWithNormals::Ptr& cloud_source) {
  view_->removePointCloud("source");
  view_->removePointCloud("target");

  PointCloudColorHandlerGenericField<PointNormalT> tgt_color_handler(
      cloud_target, "curvature");
  if (!tgt_color_handler.isCapable())
    PCL_WARN("Cannot create curvature color handler! \n");

  PointCloudColorHandlerGenericField<PointNormalT> src_color_handler(
      cloud_source, "curvature");
  if (!src_color_handler.isCapable())
    PCL_WARN("Cannot create curvature color handler! \n");

  view_->addPointCloud(cloud_target, tgt_color_handler, "target", vp2_);
  view_->addPointCloud(cloud_source, src_color_handler, "source", vp2_);

  // Clouds can't be moved
  view_->getCloudActorMap()->at("target").actor->PickableOff();
  view_->getCloudActorMap()->at("source").actor->PickableOff();

  view_->spinOnce();
}

////////////////////////////////////////////////////////////////////////////////
/** \brief Load a set of PCD files that we want to register together
 * \param argc the number of arguments (pass from main ())
 * \param argv the actual command line arguments (pass from main ())
 * \param models the resultant vector of point cloud datasets
 */
std::vector<PCD> IncrementalRegistration::loadData(int argc, char** argv) {
  std::string cloud_extension{".pcd"};

  std::vector<PCD> ret;

  for (int i = 1; i < argc; i++) {
    std::string fname{argv[i]};

    if (!fs::exists(fname)) {
      PCL_ERROR("%s does not exists \n", fname.c_str());
      continue;
    }

    // check that the argument is a pcd file
    if (boost::iequals(fs::extension(fname), cloud_extension)) {
      // Load the cloud and saves it into ret
      auto m = PCD{};
      m.f_name = fname;

      pcl::io::loadPCDFile(fname, *m.cloud);
      // remove NAN points from the cloud
      auto indices = std::vector<int>{};
      pcl::removeNaNFromPointCloud(*m.cloud, *m.cloud, indices);

      ret.push_back(m);
    } else {
      PCL_ERROR("%s does is not a valid file \n", fname.c_str());
    }
  }
  return ret;
}

////////////////////////////////////////////////////////////////////////////////
/** \brief Align a pair of PointCloud datasets and return the result
 * \param cloud_src the source PointCloud
 * \param cloud_tgt the target PointCloud
 * \param output the resultant aligned source PointCloud
 * \param final_transform the resultant transform between source and target
 */
PCD IncrementalRegistration::pairAlign(const PointCloud::Ptr& cloud_src,
                                       const PointCloud::Ptr& cloud_tgt,
                                       double leaf_size, bool downsample) {
  // Downsample for consistency and speed note enable this for large datasets
  PointCloud::Ptr src{boost::make_shared<PointCloud>()};
  PointCloud::Ptr tgt{boost::make_shared<PointCloud>()};
  auto grid = pcl::VoxelGrid<PointT>{};
  if (downsample) {
    grid.setLeafSize(leaf_size, leaf_size, leaf_size);
    grid.setInputCloud(cloud_src);
    grid.filter(*src);

    grid.setInputCloud(cloud_tgt);
    grid.filter(*tgt);

    PCL_INFO("subsampled to %d and %d points. \n", src->size(), tgt->size());
  } else {
    src = cloud_src;
    tgt = cloud_tgt;
  }

  // Compute surface normals and curvature
  auto points_with_normals_src = boost::make_shared<PointCloudWithNormals>();
  auto points_with_normals_tgt = boost::make_shared<PointCloudWithNormals>();

  auto norm_est = pcl::NormalEstimation<PointT, PointNormalT>{};
  auto tree = boost::make_shared<pcl::search::KdTree<pcl::PointXYZ>>();
  norm_est.setSearchMethod(tree);
  norm_est.setKSearch(30);

  norm_est.setInputCloud(src);
  norm_est.compute(*points_with_normals_src);
  pcl::copyPointCloud(*src, *points_with_normals_src);

  norm_est.setInputCloud(tgt);
  norm_est.compute(*points_with_normals_tgt);
  pcl::copyPointCloud(*tgt, *points_with_normals_tgt);

  // Instantiate our custom point representation (defined above) ...
  MyPointRepresentation point_representation;
  // ... and weight the 'curvature' dimension so that it is balanced against
  // x, y, and z
  float alpha[4] = {1.0, 1.0, 1.0, 1.0};
  point_representation.setRescaleValues(alpha);

  // Align
#define GICP
#define STANDARD_ICP

#ifdef GICP
  pcl::GeneralizedIterativeClosestPoint<PointNormalT, PointNormalT> reg;
#else
#  ifndef STANDARD_ICP
  pcl::IterativeClosestPointNonLinear<PointNormalT, PointNormalT> reg;
#  else
  pcl::IterativeClosestPoint<PointNormalT, PointNormalT> reg;
#  endif
#endif
  reg.setTransformationEpsilon(1e-16); // provo fino a 1e-16
  // Set the maximum distance between two correspondences (src<->tgt) to 10cm
  // Note: adjust this based on the size of your datasets
  reg.setMaxCorrespondenceDistance(MAX_DIST);
  // Set the point representation
  reg.setPointRepresentation(
      boost::make_shared<const MyPointRepresentation>(point_representation));

  reg.setInputSource(points_with_normals_src);
  reg.setInputTarget(points_with_normals_tgt);

  // Run the same optimization in a loop and visualize the results
  Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity(), prev;

  std::cout << prev << std::endl << std::endl;

  reg.setMaximumIterations(maxIterations_);
  for (int i = 0; i < 30; ++i) {
    PCL_INFO("Iteration Nr. %d. \n", i);

    // Estimate
    reg.setInputSource(points_with_normals_src);
    reg.align(*points_with_normals_src);

#ifdef GICP
    pcl::transformPointCloud(*points_with_normals_src, *points_with_normals_src,
                             reg.getFinalTransformation());
#endif

    // accumulate transformation between each Iteration
    Ti = reg.getFinalTransformation() * Ti;

    // if the difference between this transformation and the previous one
    // is smaller than the threshold, refine the process by reducing
    // the maximal correspondence distance
    if (fabs((reg.getLastIncrementalTransformation() - prev).sum()) <
        reg.getTransformationEpsilon()) {
      auto maxDist = reg.getMaxCorrespondenceDistance();
      reg.setMaxCorrespondenceDistance(0.9 * maxDist);
      PCL_INFO("change max distance to %lf \n", maxDist);
    }

    prev = reg.getLastIncrementalTransformation();

    // visualize current state
    showCloudsRight(points_with_normals_tgt, points_with_normals_src);
  }
  if (reg.hasConverged()) {
    PCL_INFO("has converged with score: %lf \n", reg.getFitnessScore());
  } else {
    PCL_INFO("has NOT converged \n");
  }

  // Get the transformation from target to source
  auto targetToSource = PCD{};
  targetToSource.Ti = Ti.inverse();

  // Transform target back in source frame
  pcl::transformPointCloud(*cloud_tgt, *(targetToSource.cloud),
                           targetToSource.Ti);

  PCL_INFO("Press q to show the final result on the original cloud. \n");
  view_->spin();
  view_->removePointCloud("source");
  view_->removePointCloud("target");

  auto cloud_tgt_h =
      PointCloudColorHandlerCustom<PointT>{targetToSource.cloud, 0, 255, 0};
  auto cloud_src_h = PointCloudColorHandlerCustom<PointT>{cloud_src, 255, 0, 0};

  view_->addPointCloud(targetToSource.cloud, cloud_tgt_h, "target", vp2_);
  view_->getCloudActorMap()->at("target").actor->PickableOff();

  view_->addPointCloud(cloud_src, cloud_src_h, "source", vp2_);
  view_->getCloudActorMap()->at("source").actor->PickableOff();

  PCL_INFO("Press q to continue the registration. \n");
  view_->spin();

  view_->removePointCloud("source");
  view_->removePointCloud("target");

  // add the source to the transformed target
  *(targetToSource.cloud) += *cloud_src;

  PCL_INFO("targetToSource: \n");
  std::cout << targetToSource.Ti << std::endl;

  return targetToSource;
}
