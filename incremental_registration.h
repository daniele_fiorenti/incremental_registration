#ifndef INCREMENTAL_REGISTRATION_H_
#define INCREMENTAL_REGISTRATION_H_

#include <memory>
#include <boost/make_shared.hpp>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/point_representation.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/filter.h>
#include <pcl/features/normal_3d.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/gicp.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/common_headers.h>
#include <vtkSmartPointer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkActor.h>

// convenient using
using PointT = pcl::PointXYZ;
using PointCloud = pcl::PointCloud<PointT>;
using PointNormalT = pcl::PointNormal;
using PointCloudWithNormals = pcl::PointCloud<PointNormalT>;
using PCLVisualizer = pcl::visualization::PCLVisualizer;

// convenient structure to handle our pointclouds
struct PCD {
  PointCloud::Ptr cloud;
  std::string f_name;
  Eigen::Matrix4f Ti;

  PCD() : cloud(boost::make_shared<PointCloud>()){};
};

class IncrementalRegistration {
 public:
  IncrementalRegistration(std::size_t maxIterations);

  std::vector<PCD> loadData(int, char**);

  void showCloudsLeft(const PCD& cloud_target, const PCD& cloud_source);

  void showCloudsRight(const PointCloudWithNormals::Ptr& cloud_target,
                       const PointCloudWithNormals::Ptr& cloud_source);

  PCD pairAlign(const PointCloud::Ptr& cloud_src,
                const PointCloud::Ptr& cloud_tgt, double leaf_size,
                bool downsample = true);

  int getLeftViewPort() { return vp1_; }
  int getRightViewPort() { return vp2_; }
  PCLVisualizer* getVisualizer() { return view_.get(); }

 private:
  std::unique_ptr<PCLVisualizer> view_;
  int vp1_;
  int vp2_;
  std::size_t maxIterations_;
};

#endif // INCREMENTAL_REGISTRATION_H_